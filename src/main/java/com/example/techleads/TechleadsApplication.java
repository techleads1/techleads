package com.example.techleads;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechleadsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechleadsApplication.class, args);
	}

}
